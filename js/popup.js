const popupLinks = document.querySelectorAll('.popup-link');
const body = document.querySelector('body');
const lockPadding = document.querySelectorAll('.lock-padding');
const lockPaddingPercent = document.querySelectorAll('.lock-padding-percent');

let unlock = true;

const timeout = 600;

//проверка наличия объектов с классом popup-link
if (popupLinks.length > 0) {
  //цикл по объектам
  for (let i = 0; i < popupLinks.length; i++) {
    //создаем объект
    const popupLink = popupLinks[i];
    //вешаем слушатель "клик"
    popupLink.addEventListener('click', function (e) {
      //получаем id popup убрав из ссылки #
      const popupName = popupLink.getAttribute('href').replace('#', '');
      const currentPopup = document.getElementById(popupName);
      popupOpen(currentPopup);
      //запрет обновления страницы
      e.preventDefault();
    })
  }
}

const popupCloseIcon = document.querySelectorAll('.close-popup');
//проверка на наличие класса
if (popupCloseIcon.length > 0) {
  for (let i = 0; i < popupCloseIcon.length; i++) {
    const el = popupCloseIcon[i];
    el.addEventListener('click', function (e) {
      //отправляем в f popupClose объект с ближайшим родителем с классом popup
      popupClose(el.closest('.popup'));
      e.preventDefault();
    })
  }
}

function popupOpen(currentPopup) {  
  if (currentPopup && unlock) {
    const popupActive = document.querySelector('.popup.open');
    if (popupActive) {
      popupClose(popupActive, false);
    } else {
      bodyLock();
    }
    currentPopup.classList.add('open');
    currentPopup.addEventListener('click', function (e) {
      if (!e.target.closest('.popup__content')) {
        popupClose(e.target.closest('.popup'));
      }
    });
  }
}

function popupClose(popupActive, doUnlock = true) {
  if (unlock) {
    popupActive.classList.remove('open');
    if (doUnlock) {
      bodyUnlock();
    }
  }
}

function bodyLock() {
  //получаем ширину полосы прокрутки
  const lockPaddingValue = window.innerWidth - document.querySelector('body').offsetWidth + 'px';
  const lockPaddingValuePercent = (0.9 - 17/window.innerWidth + 0.001)*100 + '%';

  if (lockPadding.length > 0) {
    for (let i = 0; i < lockPadding.length; i++) {
      const el = lockPadding[i];
      el.style.paddingRight = lockPaddingValue;
    }
  }
    if (lockPaddingPercent.length > 0) {
    for (let i = 0; i < lockPaddingPercent.length; i++) {
      const el = lockPaddingPercent[i];
      el.style.left = lockPaddingValuePercent;
    }
  }
  body.style.paddingRight = lockPaddingValue;
  body.classList.add('lock');

  unlock = false;
  setTimeout(function () {
    unlock = true
  }, timeout)
}

function bodyUnlock() {
  setTimeout(function () {
    if (lockPadding.length > 0) {
      for (let i = 0; i < lockPadding.length; i++) {
        const el = lockPadding[i];
        el.style.paddingRight = '0px';
      }
    }
    if (lockPaddingPercent.length > 0) {
      for (let i = 0; i < lockPaddingPercent.length; i++) {
        const el = lockPaddingPercent[i];
        el.style.left = '90%';
      }
    }
    body.style.paddingRight = '0px';
    body.classList.remove('lock');
  }, timeout);

  unlock = false;
  setTimeout(function () {
    unlock = true
  }, timeout)
}
