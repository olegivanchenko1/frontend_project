class AddCardActive {
    constructor(title, content, pic, likes) {
      this.title = title;
      this.content = content;
      this.pic = pic;
      this.likes = likes;
      this.cardTemplate = document.getElementById('card-template-main-active');
      this.sectionInner = document.getElementById('section-main-active');
      this._init()
    }
    _init() {
      this._innerData();
    }
    _innerData() {
      this.clone = this.cardTemplate.content.cloneNode(true);
      this.clone.querySelector('h2.cart-text-block__header').textContent = this.title;
      this.clone.querySelector('p.cart-text-block__text').textContent = this.content;
      this.clone.getElementById('cart__img').src = this.pic;
      this.clone.querySelector('span.cart-responce-block__counter').textContent = this.likes;
      this.sectionInner.prepend(this.clone);
    }
  }
  
  class AddCardMain extends AddCardActive {
    constructor(title, content, pic, likes) {
      super(title, content, pic, likes)
    }
    _init() {
      super._init()
    }
    _innerData() {
      this.cardTemplate = document.getElementById('card-template-main-current');
      this.sectionInner = document.getElementById('section-main-current')
      super._innerData()
    }
  }
  
  class AddCardCommon extends AddCardActive {
    constructor(title, content, pic, likes) {
      super(title, content, pic, likes)
    }
    _init() {
      super._init()
    }
    _innerData() {
      this.cardTemplate = document.getElementById('card-common');
      this.sectionInner = document.getElementById('section-common')
      super._innerData()
    }
  }
  
  
  fetch('http://localhost:3000/article/?limit=11', {
    method: 'GET'
  })
    .then(res => res.json())
    .then(articles => {
      if (articles.length === 0) {
        nowItems.init();
      } else {
        const mainActiveItem = articles[0];
        const mainItems = articles.slice(1, 5);
        const commonItems = articles.slice(5);
        const activeCard = new AddCardActive(mainActiveItem.title, mainActiveItem.content, mainActiveItem.pic, mainActiveItem.likes);
        mainItems.forEach(article => {
          const mainCard = new AddCardMain(article.title, article.content, article.pic, article.likes)
        });
        commonItems.forEach(card => {
          const commonCard = new AddCardCommon(card.title, card.content, card.pic, card.likes)
        });
      }
    })
    .catch(() => {
      nowItems.init();
    });
  
  const nowItems = {
    container: null,
    init() {
      this.container = document.getElementById('section-main');
      this.render();
    },
    createTemplate() {
      return `
        <div style="font-size: 3em">Нет статей</div>
        `
    },
    render() {
      let result = this.createTemplate();
      this.container.innerHTML = result;
    }
  }