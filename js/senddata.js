let stringUrl = document.querySelector(".string-base-64");

//Получение data-url, с base64 строкой и запись в HTML тег как хранилища
document.querySelector("#fileUpload").addEventListener("change", async function () {
  let file = document.querySelector("#fileUpload").files[0]
  let dataUrl;

  dataUrl = await loadFile(file);
  stringUrl.innerText = dataUrl;
}
)
function loadFile(file) {
  return new Promise(resolve => {
    let reader = new FileReader()
    reader.readAsDataURL(file);
    reader.onload = function (event) {
      let data = event.target.result
      resolve(data)
    }
  })
}
//Получаем объект с данными из формы
function getData(form) {
  let formData = new FormData(form);
  let obj = Object.fromEntries(formData);
  return obj;
}

document.getElementById("add-items-form").addEventListener("submit", function (event) {
  let dataUrl = document.getElementById("base64").textContent;
  let obj = getData(event.target);
  obj.pic = dataUrl;
  fetch('http://localhost:3000/article/', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(obj)
  })
    .then(response => response.json())
    .then(body => {
      console.log(body)
    });
});